import sys
from turtle import *
from emoji import *

def move_next(size):
    penup()
    right(90)
    forward(size)
    left(90)
    pendown()

penup()
goto(-350,240)


with open ("intro.txt", "r") as a:
    line_1 = a.read()
    pencolor("firebrick")
    write(line_1,font=("Time New Roman", 12, "normal"))
    move_next(120)
person = textinput("Person","Who do you think will check the mirror first ? ")

if (person.lower() == "quit"):
    sys.exit()
if (person.lower() == "harry") or (person.lower() == "potter"):
  with open ("Harry.txt", "r", encoding="utf8") as b:
      line_2 = b.read()
      pencolor("gold2")
      write(line_2,font=("Time New Roman", 12, "normal"))
      move_next(120)
  pickup = textinput("Choose","Did he think this is true?")
  if (pickup.lower() == "quit"):
      sys.exit()
  if (pickup.lower() == "yes"):
      with open ("Harry_2.txt", "r", encoding="utf8") as c:
          line_2 = c.read()
          pencolor("firebrick")
          write(line_2,font=("Time New Roman", 12, "normal"))
          move_next(120)
      pickup = textinput("Choose","If you are Ron, will you tell Dumbledore?")
      if (pickup.lower() == "quit"):
          sys.exit()
      if (pickup.lower() == "yes"):
          with open ("Harry_3.txt", "r", encoding="utf8") as f:
              line_4 = f.read()
              pencolor("gold2")
              write(line_4,font=("Time New Roman", 12, "normal"))
      else:
          with open ("Harry_4.txt", "r", encoding="utf8") as g:
              line_5 = g.read()
              write(line_5,font=("Time New Roman", 12, "normal"))
  else:
    write("Harry past out and find himself wake up in his bed.",font=("Time New Roman", 12, "normal"))

elif (person.lower() == "quit"):
    sys.exit()
elif (person.lower() == "ron") :
   with open ("Ron.txt", "r", encoding="utf8") as d:
       line_3 = d.read()
       pencolor("gold2")
       write(line_3,font=("Time New Roman", 12, "normal"))
       move_next(120)
   pickup = textinput("Choose","Did he think this is true?")
   if (pickup.lower() == "quit"):
       sys.exit()
   if (pickup.lower() == "yes"):
     with open ("Ron_2.txt", "r", encoding="utf8") as h:
          line_3 = h.read()
          pencolor("firebrick")
          write(line_3,font=("Time New Roman", 12, "normal"))
          move_next(120)
     pickup = textinput("What his next step?","A.destory the mirror  B.tell Dumbledore he wants to remove the mirror")
     if (pickup.lower() == "quit"):
         sys.exit()
     if (pickup.lower() == "a"):
        with open ("Ron_3.txt", "r", encoding="utf8") as i:
             line_3 = i.read()
             pencolor("gold2")
             write(line_3,font=("Time New Roman", 12, "normal"))
     else:
         with open ("Ron_4.txt", "r", encoding="utf8") as j:
              line_3 = j.read()
              write(line_3,font=("Time New Roman", 12, "normal"))
   else:
    write("Ron past out and find himself wake up in his bed.",font=("Time New Roman", 12, "normal"))

elif (person.lower()== "quit"):
    sys.exit()
elif (person.lower() == "hermione"):
   with open ("Hermione.txt", "r", encoding="utf8") as e:
        line_2 = e.read()
        pencolor("gold2")
        write(line_2,font=("Time New Roman", 12, "normal"))
        move_next(120)
   pickup = textinput("Choose","Did she think this is true?")
   if (pickup.lower() == "quit"):
       sys.exit()
   if (pickup.lower() == "yes"):
      with open ("Hermione_2.txt", "r", encoding="utf8") as k:
           line_2 = k.read()
           pencolor("firebrick")
           write(line_2,font=("Time New Roman", 12, "normal"))
           move_next(120)
      pickup = textinput("Choose","Will she tell Ron that she has crush on him? ")
      if (pickup.lower() == "quit"):
          sys.exit()
      if (pickup.lower() == "yes"):
         with open ("Hermione_3.txt", "r", encoding="utf8") as l:
              line_2 = l.read()
              pencolor("gold2")
              write(line_2,font=("Time New Roman", 12, "normal"))
      else:
          with open ("Hermione_4.txt", "r", encoding="utf8") as m:
               line_2 = m.read()
               write(line_2,font=("Time New Roman", 12, "normal"))

   else:
      write("Hermione past out and find himself wake up in his bed.",font=("Time New Roman", 12, "normal"))
elif (person.lower()== "quit"):
    sys.exit()
else:
  write("The Voldemort shows in the mirror.",font=("Time New Roman", 12, "normal"))

done()
